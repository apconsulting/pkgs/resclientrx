# ResClientRx

Per utilizzare il pacchetto:

Scaricare le dipendenze

> npm install

Compilare il pacchetto

> npm run build

Creare un link simbolico per il pacchetto all'interno del repository npm locale

> npm link

Nel progetto che lo deve usare lanciare questo comando che andrà a installare nel node-modules il pacchetto resclient-rx

> npm link resclient-rx

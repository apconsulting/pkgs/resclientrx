import {
  Observable,
  ReplaySubject,
  TapObserver,
  asapScheduler,
  asyncScheduler,
  combineLatest,
  concat,
  debounceTime,
  filter,
  finalize,
  first,
  map,
  observeOn,
  of,
  partition,
  queueScheduler,
  scheduled,
  share,
  subscribeOn,
  switchMap,
  take,
  tap,
  throwError,
  timeout,
  timer,
} from "rxjs";
import {
  CachedItem,
  ProperResponse,
  ResCollection,
  ResError,
  ResModel,
  ResResponseOrEvent,
  Resource,
  ResourceSet,
  Rid,
  isDataValue,
  isProperResponse,
  isResCollection,
  isResModel,
  isRid,
} from "./types";
import {
  EventInfo,
  ResEvent,
  isDeleteAction,
  isResCollectionAddEvent,
  isResCollectionRemoveEvent,
  isResDeleteEvent,
  isResModelChangeEvent,
  isResUnsubscribeEvent,
} from "./types_event";
import { ResRequest } from "./types_request";
import {
  ResResponse,
  isPayload,
  isResResponse,
  isResponseError,
} from "./types_response";
import { WebSocketChannel } from "./websocket_channel";

export enum LogLevel {
  disabled = 0,
  error = 1 << 0,
  warn = 1 << 1,
  log = 1 << 2,
  all = 7,
}

enum TapLogFlags {
  disabled = 0,
  next = 1 << 0,
  error = 1 << 1,
  complete = 1 << 2,
  subscribe = 1 << 3,
  unsubscribe = 1 << 4,
  all = 31,
}

enum ClientStatus {
  closed = 0,
  initializing = 1,
  ready = 2,
}

export type Logger = Pick<Console, "log" | "warn" | "error">;

export class ResClient {
  private ws: WebSocketChannel<ResResponseOrEvent, ResRequest>;

  // private status: ClientStatus = ClientStatus.closed

  private responseStream$: Observable<ResResponse>;
  private eventStream$: Observable<ResEvent>;

  private _msgIndex: number = 0;

  private cache: Map<string, CachedItem> = new Map();

  // Logger is the interface to a class with the log, warn and error methods
  private logger: Logger = console;
  private logMask: number = 0;

  private zeroTime: number = 0;

  // Debug mode activates logging of events and messages
  private debug: boolean = false;

  constructor(url: string, debug: boolean = false) {
    this.debug = debug;
    this.ws = new WebSocketChannel<ResResponseOrEvent, ResRequest>([url]);

    this.zeroTime = new Date().getTime();

    [this.responseStream$, this.eventStream$] = partition<
      ResResponseOrEvent,
      ResResponse
    >(this.ws, isResResponse);

    this.ws.send({
      id: this.nextID(),
      method: "version",
      params: {
        protocol: "1.2.1",
      },
    });
  }

  private log(...data: any[]) {
    if (this.debug && (this.logMask & LogLevel.log) === LogLevel.log)
      this.logger.log(...data);
  }
  private warn(...data: any[]) {
    if ((this.logMask & LogLevel.warn) === LogLevel.warn)
      this.logger.warn(...data);
  }
  private error(...data: any[]) {
    if ((this.logMask & LogLevel.error) === LogLevel.error)
      this.logger.error(...data);
  }

  // Provides a useful operator that acts as tap() and logs specified events to the logger
  private tapLog<T = any>(
    name?: string,
    flags: number = TapLogFlags.all,
  ): ReturnType<typeof tap<T>> {
    if (!this.debug) return (v) => v;

    const obs: Partial<TapObserver<any>> = {};

    if ((flags & TapLogFlags.next) === TapLogFlags.next)
      obs.next = (v) => {
        this.log(
          new Date().getTime() -
            this.zeroTime +
            " - " +
            (name ? `%cNext %c[${name}]` : `%cNext`),
          `color: black; font-weight: bold`,
          `color: black;`,
          v,
        );
      };

    if ((flags & TapLogFlags.error) === TapLogFlags.error)
      obs.error = (err) => {
        this.log(
          new Date().getTime() -
            this.zeroTime +
            " - " +
            (name ? `%cError %c[${name}]` : `%cError`),
          `color: red; font-weight: bold`,
          `color: red;`,
          err,
        );
      };

    if ((flags & TapLogFlags.complete) === TapLogFlags.complete)
      obs.complete = () => {
        this.log(
          new Date().getTime() -
            this.zeroTime +
            " - " +
            (name ? `%cComplete %c[${name}]` : `%cComplete`),
          `color: Blue; font-weight: bold`,
          `color: Blue;`,
        );
      };

    if ((flags & TapLogFlags.subscribe) === TapLogFlags.subscribe)
      obs.subscribe = () => {
        this.log(
          new Date().getTime() -
            this.zeroTime +
            " - " +
            (name ? `%cSubscribe %c[${name}]` : `%cSubscribe`),
          `color: Green; font-weight: bold`,
          `color: Green;`,
        );
      };

    if ((flags & TapLogFlags.unsubscribe) === TapLogFlags.unsubscribe)
      obs.unsubscribe = () => {
        this.log(
          new Date().getTime() -
            this.zeroTime +
            " - " +
            (name ? `%cUnsubscribe %c[${name}]` : `%cUnsubscribe`),
          `color: Navy; font-weight: bold`,
          `color: Navy;`,
        );
      };

    return tap(obs);
  }

  // Generate the next id to use
  private nextID(): number {
    return ++this._msgIndex;
  }

  private sendRequest<Req extends ResRequest>(
    req: Req,
  ): Observable<ProperResponse<Req>> {
    if (req.id === undefined) throw "Missing request ID";

    return this.responseStream$.pipe(
      tap({
        subscribe: () => {
          this.ws.send(req);
        },
      }),
      first((resp) => resp.id == req.id),
      timeout(5000),

      map((resp) => {
        if (isResponseError(resp)) {
          const err: ResError = {
            code: resp.error.code,
            message: resp.error.message,
            data: resp.error.data,
            rid: req.method,
          };

          throw err;
        }

        // Verifica che request e response corrispondano.
        if (!isProperResponse(req, resp)) {
          this.error("Wrong response", req, resp);
          throw new Error("Wrong response");
        } else return resp;
      }),
    );
  }

  // Parses a ResourceSet adding all models and collections to the Cache. If an item is
  // already in cache, trusts Resgate judgment and throws an exception. All the errors
  // included in the ResourceSet are saved in cache as throwErrors() so that they are
  // instantly thrown when accessed
  private parseResourceSet(
    rs: ResourceSet,
    rid?: Rid,
  ): ResModel | ResCollection | undefined {
    // Basta provare a togliere il valore di ritorno da sta funzione! Serve perché quando
    // sto creando la pipeline di Request verso il server la salvo in cache come
    // startingValue$.pipe(map(...)) e nella map sostituisco la Response col valore reale,
    // quindi qui ho bisogno che mi sia ritornato quel valore. Se proprio, valutare un
    // metodo alternativo di effettuare le richieste che non metta la request in cache
    // come startingValue$.
    let root: ResModel | ResCollection | undefined;

    if (rs.models) {
      for (const [key, val] of Object.entries(rs.models)) {
        const model: ResModel = {
          data: val,
          info: undefined,
        };

        if (rid && key === rid.rid) root = model;
        else {
          this.addToCache(key, of(model), false);
        }
      }
    }

    if (rs.collections) {
      for (const [key, val] of Object.entries(rs.collections)) {
        const collection: ResCollection = {
          data: val,
          info: undefined,
        };

        if (rid && key === rid.rid) root = collection;
        else {
          this.addToCache(key, of(collection), false);
        }
      }
    }

    if (rs.errors)
      for (const [key, val] of Object.entries(rs.errors)) {
        const err: ResError = {
          code: val.code,
          message: val.message,
          data: val.data,
          rid: key,
        };

        this.addToCache(
          key,
          scheduled(
            throwError(() => err),
            asyncScheduler,
          ),
          false,
        );
      }

    return root;
  }

  private addToCache(
    rid: string,
    startingValue$: Observable<ResModel | ResCollection>,
    isDirect: boolean,
  ): void {
    if (this.cache.has(rid)) throw `Resource already in cache: ${rid}`;

    const shortName = rid.substring(rid.lastIndexOf("."), rid.length);

    const item: CachedItem = {
      cachedValue: undefined,
      directlySubscribed: isDirect,
      obs$: concat(
        // Carico il valore iniziale
        startingValue$.pipe(
          tap({
            next: (v) => {
              if (!isResCollection(v) && !isResModel(v))
                throw `Starting value for ${rid} is neither a collection nor a model`;

              item.cachedValue = v;
            },
            error: () => {
              this.cache.delete(rid);
            },
          }),
        ),

        // Resto in ascolto degli eventi
        this.eventStream$.pipe(
          // Filtro solo gli eventi che interessano questa risorsa
          filter(
            (ev) => ev.event.substring(0, ev.event.lastIndexOf(".")) === rid,
          ),

          // Schedulo le emissioni sul queueScheduler. Questo punto è importante in quanto
          // se ci fosse un'emissione rapida di eventi, più rapida di quanto la pipe
          // riesca a processarli (formando così una sorta di "coda" di attesa),  il
          // comportamento normale è quello di emetterli in maniera sincrona uno dopo
          // l'altro. Questo però comporta che un eventuale nuovo evento verrebbe
          // processato prima che la cache abbia avuto il tempo di reagire ai cambiamenti
          // dovuti all'ultimo evento emesso. Questo è problematico nel caso in cui il
          // nuovo evento porti con se un ResourceSet che contiene modelli o collection
          // che avrebbero già dovuto essere stati eliminati da un precedente evento nella
          // coda. Il caso tipico è quello di una collection che riordina i propri
          // elementi.

          // Aggiungendo questo operatore spezzo la sincronicità della coda e permetto ad
          // ogni singola emissione di operare i cambiamenti dovuti prima di prendere in
          // carico il prossimo evento.
          observeOn(queueScheduler),

          this.tapLog("Event", TapLogFlags.next),

          // Applico gli eventi a cachedResource e lo invio downstream
          map((ev) => {
            if (!item.cachedValue)
              throw `Received an event for ${rid} with an undefined cached value. This is probably caused by events being emitted before the initial value has been fetched`;

            if (isResDeleteEvent(ev)) {
              this.log(
                `Received delete event for resource ${rid}. It is still considered subscribed, but no more events will be emitted for it.`,
              );

              return undefined;
            } else if (isResUnsubscribeEvent(ev)) {
              item.directlySubscribed = false;
              this.log(
                `Received unsubscribe event for resource ${rid}. It is still considered subscribed, but no unsubscribe request will be sent when unsubscribed, unless re-subscribed.`,
              );
              return undefined;
            } else if (isResCollection(item.cachedValue)) {
              if (isResCollectionAddEvent(ev)) {
                this.parseResourceSet(ev.data);

                item.cachedValue.data.splice(ev.data.idx, 0, ev.data.value);
              } else if (isResCollectionRemoveEvent(ev)) {
                item.cachedValue.data.splice(ev.data.idx, 1);
              } else
                throw `Wrong event type for collection ${rid} - ${ev.event}`;
            } else {
              if (!isResModelChangeEvent(ev)) {
                throw `Wrong event type for model ${rid} - ${ev.event}`;
              }

              this.parseResourceSet(ev.data);

              for (const [key, val] of Object.entries(ev.data.values)) {
                if (isDeleteAction(val)) {
                  delete item.cachedValue.data[key];
                } else {
                  item.cachedValue.data[key] = val;
                }
              }
            }

            item.cachedValue.info = ev.info;
            return item.cachedValue;
          }),

          // Mando giù il valore solo se l'evento l'ha modificato
          filter<
            ResModel | ResCollection | undefined,
            ResModel | ResCollection
          >((v): v is ResModel | ResCollection => {
            return v !== undefined;
          }),

          // tap({
          //   subscribe: () => {
          //     this.log(
          //       `%c MI SONO SOTTOSCRITTO AGLI EVENTI DI ${rid} `,
          //       "color: yellow; background-color: #555555",
          //     );
          //   },
          // }),

          // Questo scheduler evita che avvenga una sottoscrizione allo stream degli
          // eventi prima che lo startingValue venga usato ed emesso. In sostanza, evita
          // che una GetRequest, che per sua natura esce alla prima emissione (ovvero è
          // interessata solo allo startingValue$), si sottoscriva agli eventi degli
          // eventuali altri rid interni
          subscribeOn(asapScheduler),
          // observeOn(asyncScheduler)
        ),
      ).pipe(
        this.tapLog(`Pre Finalize ${shortName}`, TapLogFlags.unsubscribe),

        finalize(() => {
          if (item.directlySubscribed)
            this.sendRequest({
              id: this.nextID(),
              method: `unsubscribe.${rid}`,
            }).subscribe({
              error: (err) => {
                // A questo punto non c'è piu nessuno di sottoscritto a cui notificare
                // l'errore, quindi lo loggo e tanti saluti
                this.error(`Error while unsubscribing from ${rid} -`, err);
              },
            });

          this.log(`%c ---> ${rid}`, "background-color:red");
          this.cache.delete(rid);
        }),

        this.tapLog(
          `Shared ${shortName}`,
          TapLogFlags.subscribe | TapLogFlags.unsubscribe,
        ),

        share({
          connector: () => {
            this.log(`%c CONNECTOR ${rid}`, "background-color: #DDDDDD");
            return new ReplaySubject(1);
          },
          resetOnRefCountZero: () => {
            // Questo a quanto pare è il modo idiomatico di rxjs per dire "conta quanti
            // subscriber hai usando questo scheduler invece che in maniera sincrona"
            return scheduled(
              of(0) /* .pipe(
                tap(() => {
                  this.log(
                    `%c EMESSO REFCOUNT ZERO PER ${shortName}`,
                    "color: aqua",
                  );
                }),
              ) */,
              queueScheduler,
            );
          },
        }),
      ),
    };

    this.log(`%c @ <--- ${rid}`, "background-color: lime");
    this.cache.set(rid, item);
  }

  private valueFromCache<T>(
    rid: string,
    visited: {
      [k: string]: {
        value: any;
        rids: Set<string>;
      };
    } = {},
  ): Observable<Resource> {
    // Verifico di avere l'item richiesto in cache
    const item = this.cache.get(rid);

    if (item === undefined) {
      return throwError(
        () => new Error(`Tried to valueFromCache ${rid} which is not in cache`),
      );
    }

    // Debug
    const shortName = rid.substring(rid.lastIndexOf("."), rid.length);

    visited[rid] = {
      value: {},
      rids: new Set(),
    };

    return item.obs$.pipe(
      this.tapLog(
        `Pre ${shortName}`,
        TapLogFlags.subscribe | TapLogFlags.unsubscribe,
      ),

      switchMap<ResModel | ResCollection, Observable<Resource>>(
        (res: ResCollection | ResModel<T>) => {
          function cleanseVisited(rid: string) {
            for (const remote of visited[rid]?.rids) {
              cleanseVisited(remote);
            }
            delete visited[rid];
          }

          cleanseVisited(rid);

          visited[rid] = {
            value: {},
            rids: new Set(),
          };

          let info: EventInfo | undefined = res.info;
          const assembled: Record<string, Observable<any>> = {};

          for (const [idx, val] of Object.entries(res.data)) {
            if (isDataValue(val)) {
              assembled[idx] = of(val.data);
            } else if (isRid(val) && !val.soft) {
              if (visited[val.rid]) {
                // Se ho già visitato il rid, emetto una referenza al placeholder relativo a
                // quel rid che verrà valorizzato in futuro quando anch'esso emetterà il
                // proprio valore (nel caso di ciclicità, questo avviene strettamente dopo
                // l'emissione dell'observable che sto trattando quindi devo agire
                // preventivamente in qualche modo).
                assembled[idx] = of(visited[val.rid].value);
              } else {
                visited[rid].rids.add(val.rid);
                assembled[idx] = this.valueFromCache(val.rid, visited).pipe(
                  map((mc) => {
                    if (mc.info?.external)
                      if (info) info.external = mc.info.external;
                      else info = { external: mc.info.external };
                    return mc.data;
                  }),
                );
              }
            } else {
              assembled[idx] = of(val);
            }
          }

          if (isResModel(res))
            return combineLatest(assembled).pipe(
              map((data) => {
                return {
                  data,
                  info,
                };
              }),
            );
          else {
            if (Object.values(assembled).length === 0)
              return of({
                data: [],
                info,
              });

            return combineLatest(Object.values(assembled)).pipe(
              map((data) => {
                return {
                  data,
                  info,
                };
              }),
            );
          }
        },
      ),

      // this.tapLog(
      //   `Post ${shortName}`,
      //   TapLogFlags.subscribe | TapLogFlags.unsubscribe | TapLogFlags.next,
      // ),

      tap((v) => {
        if (isResCollection(v)) {
          // Direttamente dal Necronomicon:
          if (!(visited[rid] instanceof Array)) {
            Object.setPrototypeOf(visited[rid].value, Array.prototype);
          }

          visited[rid].value.splice(0, visited[rid].value.length);

          for (const k of v.data) {
            visited[rid].value.push(k);
          }
        } else {
          for (const k of Object.keys(visited[rid].value))
            delete visited[rid].value[k];
          Object.assign(visited[rid].value, v.data);
        }

        // this.log(
        //   `%cVisiting ${shortName}`,
        //   "color: orange",
        //   this.deepCopy(visited)
        // );
      }),
    );
  }

  deepCopy(v: any, visited: Map<any, any> = new Map()): any {
    if (typeof v !== "object" || v === null) return v;

    var producedValue: Array<any> | Record<string, any> | undefined;

    if (Array.isArray(v) || v instanceof Array) {
      producedValue = [];
      visited.set(v, producedValue);

      for (const val of v) {
        if (typeof val === "object") {
          const cached = visited.get(val);

          if (cached !== undefined) producedValue.push(cached);
          else {
            const copy = this.deepCopy(val, visited);
            producedValue.push(copy);
          }
        } else producedValue.push(val);
      }
    } else {
      producedValue = {};
      visited.set(v, producedValue);

      for (const [key, val] of Object.entries(v)) {
        if (typeof val === "object") {
          const cached = visited.get(val);

          if (cached !== undefined) producedValue[key] = cached;
          else {
            producedValue[key] = this.deepCopy(val, visited);
          }
        } else producedValue[key] = val;
      }
    }

    return producedValue;
  }

  // Produce un observable che emette il valore di una determinata risorsa dalla cache,
  // applicando un piccolo debounce per evitare spam nel caso di eventi ravvicinati e
  // creando una copia deep del valore emesso
  private produce<T>(rid: string): Observable<Resource<T>> {
    let accumulatedExternal: boolean = false;

    return this.valueFromCache<T>(rid).pipe(
      tap((v) => {
        if (v.info) accumulatedExternal ||= v.info.external;
      }),
      debounceTime(50),
      map((v) => {
        if (v.info) v.info.external = accumulatedExternal;
        accumulatedExternal = false;
        return v;
      }),

      // Questo share NON serve a condividere il valore, dato che la produce viene
      // reinvocata ogni volta che un consumer accede ad una risorsa, ma serve
      // semplicemente a mantenere la sottoscrizione viva anche dopo che viene
      // effettuata una unsubscribe. In questo modo quando l'ultimo consumer chiude la
      // propria sottoscrizione, attendo un tempo prestabilito prima di inviare la
      // richiesta di Unsubscribe a Resgate, ottimizzando i casi piuttosto comuni di
      // consumer che disdicono per poi reiscriversi immediatamente dopo.
      share({
        connector: () => new ReplaySubject(1),
        resetOnRefCountZero: () => {
          return timer(500);
        },
      }),
      map((v) => this.deepCopy(v)),
    );
  }

  // Invoca un metodo di un servizio. Il valore di ritorno può essere un payload singolo
  // oppure un Rid. Nel caso di un Rid, la risorsa è da considerarsi sottoscritta
  // direttamente, quindi va trattata come se fosse ottenuta da una Observe (ie. è
  // necessaria una unsubscribe esplicita per evitare un memory leak)
  call<T>(rid: string, params?: any): Observable<Resource<T>> {
    return this.sendRequest({
      id: this.nextID(),
      method: `call.${rid}`,
      params,
    }).pipe(
      switchMap((resp) => {
        if (isPayload(resp.result))
          return of({ data: resp.result.payload, info: undefined });
        if (!isRid(resp.result))
          throw "Malformed call response (missing both payload and rid)";

        const root = this.parseResourceSet(resp.result, resp.result);

        if (root !== undefined)
          this.addToCache(resp.result.rid, of(root), true);

        const val = this.cache.get(resp.result.rid);

        if (val === undefined)
          throw "Missing requested resource in either response or cache";
        else val.directlySubscribed = true;

        return this.produce<T>(resp.result.rid);
      }),
    );
  }

  // Ottiene un valore una singola volta, senza sottoscrizione
  get<T>(rid: string): Observable<Resource<T>> {
    if (this.cache.has(rid))
      return this.valueFromCache<T>(rid).pipe(
        // tap((v) => {
        //   this.log(
        //     `%cLast Visit ${rid}`,
        //     "color: orange",
        //     this.deepCopy(visited)
        //   );
        // }),
        take(1),
        map((v) => {
          return this.deepCopy(v);
        }),
      );

    const startingValue$ = this.sendRequest({
      id: this.nextID(),
      method: `get.${rid}`,
    }).pipe(
      map((resp) => {
        const root = this.parseResourceSet(resp.result, { rid });

        if (root === undefined) throw "Missing requested resource in response";

        return root;
      }),
    );

    // TODO: Qua in realtà non ho bisogno di mettere nulla in cache dato che una get non
    // si sottoscrive alle notifiche, ma facendo cosi sfrutto la logica di costruzione del
    // valueFromCache evitando altri sbattimenti (sono molti, ci ho provato...).
    // Se dovesse dar problemi valutare di evitare il passaggio in cache ricostruendo la
    // pipe specifica per la richiesta Get
    this.addToCache(rid, startingValue$, false);
    return this.valueFromCache<T>(rid).pipe(
      // tap((v) => {
      //   this.log(
      //     `%cLast Visit ${rid}`,
      //     "color: orange",
      //     this.deepCopy(visited)
      //   );s
      // }),
      take(1),
      map((v) => this.deepCopy(v)),
    );
  }

  // Ottiene un valore ed effettua una sottoscrizione ai suoi cambiamenti. Quando questo
  // valore cambia, riottiene nuovamente il valore. Ogni copia emessa è invariante
  observe<T>(rid: string): Observable<Resource<T>> {
    let item = this.cache.get(rid);

    if (item) {
      if (!item.directlySubscribed) {
        // Se avevo già il valore in cache, ma la sottoscrizione era di tipo indiretto,
        // effettuo una SubscribeRequest per questa risorsa indicando a Resgate che la
        // sottoscrizione dev'essere da ora in poi considerata diretta. Questo inoltre
        // sottintende che la relativa UnsubscribeRequest verrà inviata solamente dopo che
        // tutte le sottoscrizioni (dirette e indirette) a quest'observable sono
        // terminate.

        // TODO: Sento puzza di race condition... Ad esempio se setto directlySubscribed a
        // true, invio la sendRequest relativa, prima che questa sia effettiva tutti gli
        // osservatori si unsubbano causando una UnsubscribeRequest perché la risorsa è
        // ora considerata diretta "client-side", ma non per il server dato che la
        // sendRequest è ancora in-flight... E' anche vero che non so come Resgate gestice
        // la medesima race condition lato server, probabilmente non mi devo nemmeno
        // preoccupare...
        item.directlySubscribed = true;

        // TODO: Faccio sta cosa per aver qualcuno che viene notificato nel caso ci sia un
        // errore nella richiesta di sottoscrizione, ma tecnicamente potrei ritornare
        // direttamente il valueFromCache, senza aspettare che la sendRequest emetta.
        // Quest'ultima potrebbe avere vita propria ma va gestito il caso di errore
        return this.sendRequest({
          id: this.nextID(),
          method: `subscribe.${rid}`,
        }).pipe(
          tap({
            error: () => {
              const item = this.cache.get(rid);
              if (!item) {
                this.error("Something went horribly wrong...");
                return;
              }

              item.directlySubscribed = false;
            },
          }),
          switchMap(() => this.produce<T>(rid)),
        );
      } else return this.produce<T>(rid);
    }

    // TODO: Gestire le richieste in-flight, se richiedo velocemente la stessa risorsa due
    // volte, essendo lo startingValue$ la request, facendo due volte la subscribe invia
    // due volte la request.
    // UPDATE: Non è vero per niente! O almeno non dovrebbe essere vero, dato che per
    // raggiunere lo startingValue$ in prima istanza passo da una shareReplay che
    // impedisce che la concat(startingValue$, eventStream$) possa essere sottoscritta più
    // di una singola volta.

    // La risorsa non è presente in cache, la aggiungo alla cache con una SubscribeRequest
    // come startingValue$ e quindi ritorno un producer per questa risorsa
    this.addToCache(
      rid,
      this.sendRequest({
        id: this.nextID(),
        method: `subscribe.${rid}`,
      }).pipe(
        map((resp) => {
          const root = this.parseResourceSet(resp.result, { rid });
          if (root === undefined)
            throw `Missing resource ${rid} in either response or cache`;

          return root;
        }),
      ),
      true,
    );

    return this.produce<T>(rid);
  }

  // Effettua una richiesta di tipo Auth. Il valore di ritorno può essere un payload
  // singolo oppure un Rid. Nel caso di un Rid, la risorsa è da considerarsi sottoscritta
  // direttamente, quindi va trattata come se fosse ottenuta da una Observe (ie. è
  // necessaria una unsubscribe esplicita per evitare un memory leak)
  auth<T>(method: string, params?: any): Observable<Resource<T>> {
    return this.sendRequest({
      id: this.nextID(),
      method: `auth.${method}`,
      params,
    }).pipe(
      switchMap((resp) => {
        if (isPayload(resp.result)) return of(resp.result.payload);
        if (!isRid(resp.result)) throw "Malformed auth response";

        const root = this.parseResourceSet(resp.result, resp.result);

        if (root !== undefined)
          this.addToCache(resp.result.rid, of(root), true);

        const val = this.cache.get(resp.result.rid);

        if (val === undefined)
          throw "Missing auth resource in either response or cache";
        else val.directlySubscribed = true;

        return this.produce<T>(resp.result.rid);
      }),
    );
  }

  setLogger(logger: Logger) {
    this.logger = logger;
  }

  setLogMask(mask: number) {
    this.logMask = mask;
  }
}

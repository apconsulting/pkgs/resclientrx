import { Observable } from "rxjs";
import { EventInfo, ResEvent } from "./types_event";
import {
  AuthRequest,
  CallRequest,
  GetRequest,
  ResRequest,
  SubscribeRequest,
  UnsubscribeRequest,
  VersionRequest,
  isAuthRequest,
  isCallRequest,
  isGetRequest,
  isSubscribeRequest,
  isUnsubscribeRequest,
  isVersionRequest,
} from "./types_request";
import {
  AuthResponse,
  CallResponse,
  ErrorResponse,
  GetResponse,
  ResResponse,
  SubscribeResponse,
  UnsubscribeResponse,
  VersionResponse,
  isAuthResponse,
  isCallResponse,
  isGetResponse,
  isSubscribeResponse,
  isUnsubscribeResponse,
  isVersionResponse,
} from "./types_response";

// export type ResModel<T> = Record<string, ResValue>;
export type Resource<T = any> = {
  data: T;
  info: EventInfo | undefined;
};

export interface ResModel<T = any> {
  data: {
    [key in keyof T]: ResValue;
  };
  info: EventInfo | undefined;
}

export interface ResCollection {
  data: Array<ResValue>;
  info: EventInfo | undefined;
}

// export type ResModelValue<T extends ResModel<any>> = {
//   -readonly [key in keyof T]: T[key] extends Rid
//     ? any
//     : T[key] extends DataValue
//     ? DataValue["data"]
//     : T[key];
// };

// export type ResCollectionValue<T extends ResCollection> = Array<T>;

export interface ResourceSet {
  models?: {
    [rid: string]: ResModel["data"];
  };

  collections?: {
    [rid: string]: ResCollection["data"];
  };

  errors?: {
    [rid: string]: ResError;
  };
}

export interface DataValue {
  data: any;
}

export interface Rid {
  rid: string;
  soft?: boolean;
}

export type ResValue = string | number | true | false | null | DataValue | Rid;

export type ResResponseOrEvent = ResResponse | ResEvent;

export interface CachedItem<T = any> {
  directlySubscribed?: boolean;
  obs$: Observable<ResModel<T> | ResCollection>;
  cachedValue: ResModel | ResCollection | undefined;
}

export type ResError = ErrorResponse["error"] & {
  rid?: string;
};

export type ProperResponse<T extends ResRequest> = T extends VersionRequest
  ? VersionResponse
  : T extends AuthRequest
  ? AuthResponse
  : T extends CallRequest
  ? CallResponse
  : T extends GetRequest
  ? GetResponse
  : T extends SubscribeRequest
  ? SubscribeResponse
  : T extends UnsubscribeRequest
  ? UnsubscribeResponse
  : never;

// Type guards

export function isDataValue(v: ResValue): v is DataValue {
  return v != null && typeof v === "object" && v.hasOwnProperty("data");
}

export function isRid(v: ResValue): v is Rid {
  return v != null && typeof v === "object" && v.hasOwnProperty("rid");
}

export function isResModel(v: ResModel | ResCollection): v is ResModel {
  return v.hasOwnProperty("data") && !Array.isArray(v.data);
}

export function isResError(err: any): err is ResError {
  return err.hasOwnProperty("code") && err.hasOwnProperty("message");
}

export function isResCollection(
  v: ResModel | ResCollection,
): v is ResCollection {
  return v.hasOwnProperty("data") && Array.isArray(v.data);
}

export function isProperResponse<Q extends ResRequest>(
  req: Q,
  resp: ResResponse,
): resp is ProperResponse<Q> {
  return !(
    (isVersionRequest(req) && !isVersionResponse(resp)) ||
    (isAuthRequest(req) && !isAuthResponse(resp)) ||
    (isCallRequest(req) && !isCallResponse(resp)) ||
    (isGetRequest(req) && !isGetResponse(resp)) ||
    (isSubscribeRequest(req) && !isSubscribeResponse(resp)) ||
    (isUnsubscribeRequest(req) && !isUnsubscribeResponse(resp))
  );
}

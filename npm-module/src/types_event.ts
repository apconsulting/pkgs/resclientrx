import { ResError, ResResponseOrEvent, ResValue, ResourceSet } from "./types";

export interface DeleteAction {
  action: "delete";
}

export type ChangeEventValue = ResValue | DeleteAction;
export interface ChangeEventObject {
  values: {
    [key: string]: ChangeEventValue;
  };
}

export interface AddEventObject {
  idx: number;
  value: ResValue;
}

export interface EventInfo {
  external: boolean;
}

export interface BaseEvent {
  event: string;
  info?: EventInfo;
}

export interface ResModelChangeEvent extends BaseEvent {
  data: ChangeEventObject & ResourceSet;
}

// {"event":"ms_emme2.slabs_by_ul.8.add","data":{"idx":11,"value":{"rid":"ms_emme2.slab.MDQ3MDktNDk=","soft":true}}}
export interface ResCollectionAddEvent extends BaseEvent {
  data: AddEventObject & ResourceSet;
}

export interface ResCollectionRemoveEvent extends BaseEvent {
  data: {
    idx: number;
  };
}

export interface ResDeleteEvent extends BaseEvent {
  // Without this property typescript narrows ResDeleteEvent to BaseEvent and in doing so
  // considers type guarding for ResDeleteEvent equivalent to type guarding for BaseEvent.
  // This means that when the guard is false, the event is narrowed to "never" because
  // every type of event extends BaseEvent. Adding a specific property to this interface
  // prevents typescript from being too zealous.
  data: null;
}

export interface ResUnsubscribeEvent extends BaseEvent {
  data: {
    reason: ResError;
  };
}

export interface ResCustomEvent<T = any> extends BaseEvent {
  data: T;
}

export type ResEvent =
  | ResModelChangeEvent
  | ResCollectionAddEvent
  | ResCollectionRemoveEvent
  | ResDeleteEvent
  | ResCustomEvent
  | ResDeleteEvent;

// Type Guards

export function isResEvent(msg: ResResponseOrEvent): msg is ResEvent {
  return msg.hasOwnProperty("event") && !msg.hasOwnProperty("id");
}

export function isResDeleteEvent(ev: ResEvent): ev is ResDeleteEvent {
  return ev.event.endsWith(".delete");
}

export function isResUnsubscribeEvent(ev: ResEvent): ev is ResUnsubscribeEvent {
  return ev.event.endsWith(".unsubscribe");
}

export function isResModelChangeEvent(ev: ResEvent): ev is ResModelChangeEvent {
  return (
    ev.event.endsWith(".change") &&
    ev.hasOwnProperty("data") &&
    (ev as ResModelChangeEvent).data.hasOwnProperty("values")
  );
}

export function isResCollectionAddEvent(
  ev: ResEvent,
): ev is ResCollectionAddEvent {
  return (
    ev.event.endsWith(".add") &&
    ev.hasOwnProperty("data") &&
    (ev as ResCollectionAddEvent).data.hasOwnProperty("idx") &&
    (ev as ResCollectionAddEvent).data.hasOwnProperty("value")
  );
}

export function isResCollectionRemoveEvent(
  ev: ResEvent,
): ev is ResCollectionRemoveEvent {
  return (
    ev.event.endsWith(".remove") &&
    ev.hasOwnProperty("data") &&
    (ev as ResCollectionRemoveEvent).data.hasOwnProperty("idx")
  );
}

export function isDeleteAction(v: ChangeEventValue): v is DeleteAction {
  return (
    v !== null &&
    typeof v === "object" &&
    v.hasOwnProperty("action") &&
    (v as { action: any }).action === "delete"
  );
}

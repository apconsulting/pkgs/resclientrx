export interface BaseRequest {
  id: number;
  method: string;
}

// VersionRequest
export interface VersionRequest extends BaseRequest {
  method: "version";
  params?: {
    protocol: `${number}.${number}.${number}`;
  };
}

// AuthRequest
export interface AuthRequest extends BaseRequest {
  method: `auth.${string}`;
  params: any;
}

// CallRequest
export interface CallRequest extends BaseRequest {
  method: `call.${string}`;
  params?: any;
}

// GetRequest
export interface GetRequest extends BaseRequest {
  method: `get.${string}`;
}

// SubScribeRequest
export interface SubscribeRequest extends BaseRequest {
  method: `subscribe.${string}`;
}

// UnsubscribeRequest
export interface UnsubscribeRequest extends BaseRequest {
  method: `unsubscribe.${string}`;
}

export type ResRequest =
  | VersionRequest
  | AuthRequest
  | CallRequest
  | GetRequest
  | SubscribeRequest
  | UnsubscribeRequest;

export function isVersionRequest(req: ResRequest): req is VersionRequest {
  return req.hasOwnProperty("method") && req.method == "version";
}

export function isAuthRequest(req: ResRequest): req is AuthRequest {
  return req.hasOwnProperty("method") && req.method.startsWith("auth");
}

export function isCallRequest(req: ResRequest): req is CallRequest {
  return req.hasOwnProperty("method") && req.method.startsWith("call");
}

export function isGetRequest(req: ResRequest): req is GetRequest {
  return req.hasOwnProperty("method") && req.method.startsWith("get");
}

export function isSubscribeRequest(req: ResRequest): req is SubscribeRequest {
  return req.hasOwnProperty("method") && req.method.startsWith("subscribe");
}

export function isUnsubscribeRequest(
  req: ResRequest
): req is UnsubscribeRequest {
  return req.hasOwnProperty("method") && req.method.startsWith("unsubscribe");
}

export type RequestWithoutID<Q extends ResRequest> = Omit<Q, "id"> & {
  id?: number;
};

import { ResResponseOrEvent as ResWsPayload, ResourceSet, Rid } from "./types";

// BaseResponse contains all the common properties
export interface BaseResponse {
  id: number;
}

export interface Payload {
  payload: any;
}

export interface VersionResponse extends BaseResponse {
  result: {
    protocol: `${number}.${number}.${number}`;
  };
}

// AuthResponse defines a response from an auth request
export interface AuthResponse extends BaseResponse {
  result: Payload | (Rid & Omit<ResourceSet, "errors">);
}

// CallResponse defines a response to a call request
export interface CallResponse extends BaseResponse {
  result: Payload | (Rid & ResourceSet);
}

// GetResponse defines a response to a get request
export interface GetResponse extends BaseResponse {
  result: ResourceSet;
}

// SubscribeResponse defines a response to a subscribe request
export interface SubscribeResponse extends BaseResponse {
  result: ResourceSet;
}

// UnsubscribeResponse
export interface UnsubscribeResponse extends BaseResponse {}

// ResponseError
export interface ErrorResponse extends BaseResponse {
  error: {
    code: string;
    message: string;
    data?: any;
  };
}

export type ResResponse =
  | VersionResponse
  | AuthResponse
  | CallResponse
  | GetResponse
  | SubscribeResponse
  | UnsubscribeResponse
  | ErrorResponse;

// Type guards

export function isResourceSet(v: any): v is ResourceSet {
  return (
    Object.keys(v).length == 0 ||
    v.hasOwnProperty("models") ||
    v.hasOwnProperty("collections") ||
    v.hasOwnProperty("errors")
  );
}

export function isResResponse(msg: ResWsPayload): msg is ResResponse {
  return msg.hasOwnProperty("id") && !msg.hasOwnProperty("event");
}

export function isGetResponse(resp: ResResponse): resp is GetResponse {
  return (
    resp.hasOwnProperty("result") && isResourceSet((resp as GetResponse).result)
  );
}

export function isSubscribeResponse(
  resp: ResResponse
): resp is SubscribeResponse {
  return (
    resp.hasOwnProperty("result") &&
    isResourceSet((resp as SubscribeResponse).result)
  );
}

export function isCallResponse(resp: ResResponse): resp is CallResponse {
  return (
    resp.hasOwnProperty("result") &&
    (((resp as CallResponse).result.hasOwnProperty("payload") &&
      !(resp as CallResponse).result.hasOwnProperty("rid")) ||
      (!(resp as CallResponse).result.hasOwnProperty("payload") &&
        (resp as CallResponse).result.hasOwnProperty("rid")))
  );
}

export function isAuthResponse(resp: ResResponse): resp is AuthResponse {
  return (
    resp.hasOwnProperty("result") &&
    (((resp as AuthResponse).result.hasOwnProperty("payload") &&
      !(resp as AuthResponse).result.hasOwnProperty("rid")) ||
      (!(resp as AuthResponse).result.hasOwnProperty("payload") &&
        (resp as AuthResponse).result.hasOwnProperty("rid")))
  );
}

export function isVersionResponse(resp: ResResponse): resp is VersionResponse {
  return (
    resp.hasOwnProperty("result") &&
    (resp as VersionResponse).result.hasOwnProperty("protocol")
  );
}

export function isPayload(
  result: Payload | (Rid & ResourceSet)
): result is Payload {
  return result.hasOwnProperty("payload");
}

export function isResponseError(resp: ResResponse): resp is ErrorResponse {
  return resp.hasOwnProperty("error");
}

export function isUnsubscribeResponse(
  resp: ResResponse
): resp is UnsubscribeResponse {
  // UnsubscribeResponse has no payload to type guard against, so we check that it isn't
  // actually another kind of response
  return (
    !isGetResponse(resp) &&
    !isAuthResponse(resp) &&
    !isCallResponse(resp) &&
    !isVersionResponse(resp)
  );
}

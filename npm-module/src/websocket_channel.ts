import { Subject } from "rxjs";

export type WebSocketMessage = Parameters<WebSocket["send"]>[0];

export interface WebSocketChannelOptions<Incoming, Outgoing> {
  serializer?: (value: Outgoing) => WebSocketMessage;
  deserializer?: <T = any>(e: MessageEvent<T>) => Incoming;
}

// Wrappa una connessione WebSocket con un Subject che emette i valori ricevuti. Aggiunge
// inoltre un metodo send() per inviare dati sul canale
export class WebSocketChannel<Incoming, Outgoing> extends Subject<any> {
  private ws: WebSocket;
  // private queue$: ReplaySubject<Outgoing>;
  private queue: Outgoing[] = [];
  private opening: boolean = true;

  serializer: (value: Outgoing) => WebSocketMessage;
  deserializer: (e: MessageEvent<string | ArrayBuffer | Blob>) => Incoming;

  constructor(
    params: ConstructorParameters<typeof WebSocket>,
    options?: WebSocketChannelOptions<Incoming, Outgoing>
  ) {
    // Inizializzo il subject
    super();

    // Apro la connessione websocket coi parametri forniti
    this.ws = new WebSocket(...params);

    // Imposto serializer / deserializer forniti oppure uso JSON come default
    this.serializer = options?.serializer ?? JSON.stringify;
    this.deserializer =
      options?.deserializer ??
      ((e) => {
        if (typeof e.data !== "string")
          throw "Default deserializer can only handle string messages";

        return JSON.parse(e.data);
      });

    // Registro la callback su ricezione di un messaggio WS
    this.ws.onmessage = (e: MessageEvent<string | ArrayBuffer | Blob>) => {
      if (!this.closed) super.next(this.deserializer(e));
    };

    // Attendo che la connessione sia pienamente stabilita prima di elaborare i messaggi
    // in coda, per evitare una exception
    this.ws.onopen = () => {
      this.opening = false;

      for (const req of this.queue) {
        this.ws.send(this.serializer(req));
      }

      this.queue = [];
    };

    // Alla chiusura della connessione WS, completo il subject
    this.ws.onclose = this.complete;

    // Se ricevo un errore di connessione, lo inoltro sul subject
    this.ws.onerror = this.error;
  }

  /**
   * @deprecated For correctness, the use of next on this Subject should be avoided. Use
   * {@link send} instead */
  next = this.send;

  // Serializza e invia un messaggio sulla connessione WebSocket
  send(payload: Outgoing): void {
    if (this.closed) throw "Connection closed";

    if (this.opening) this.queue.push(payload);
    else this.ws.send(this.serializer(payload));
  }

  // Chiude il canale
  close(): void {
    // Chiudendo la connessione WebSocket scateno la callback che invoca .complete(),
    // quindi anche il Subject si chiude di conseguenza
    this.ws.close();
  }
}

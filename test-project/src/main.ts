import { ResClient } from "resclient-rx";
import { Subscription, debounceTime, tap } from "rxjs";
import "./style.css";
import typescriptLogo from "./typescript.svg";
import viteLogo from "/vite.svg";

document.querySelector<HTMLDivElement>("#rxjs-app")!.innerHTML = `
  <div>
    <a href="https://vitejs.dev" target="_blank">
      <img src="${viteLogo}" class="logo" alt="Vite logo" />
    </a>
    <a href="https://www.typescriptlang.org/" target="_blank">
      <img src="${typescriptLogo}" class="logo vanilla" alt="TypeScript logo" />
    </a>
    <h1>Vite is running</h1>
    <p class="read-the-docs">
      <button id="btn1">1)</button> Subscribe to model_simple, unsubscribe after 1s
    </p>
    <p class="read-the-docs">
      <button id="btn2">2)</button> Subscribe to collection_int and listen until <button id="unsub2">stopped</button>
    </p>
    <p class="read-the-docs">
      <button id="btn3">3)</button> Subscribe to collection_random and listen until <button id="unsub3">stopped</button> or <button id="get3">Get</button> it once
    </p>
    <p class="read-the-docs">
      <button id="btn4">4)</button> Subscribe to collection_rid and listen until <button id="unsub4">stopped</button> - Controls: <button id="dec4">&minus;</button> / <button id="inc4">&plus;</button> - <button id="get4">Get</button> it once
    </p>
    <p class="read-the-docs">
      <button id="btn5">5)</button> Subscribe to simple_model and listen until <button id="unsub5">stopped</button>
    </p>
    <p class="read-the-docs">
      <button id="btn6">6)</button> Subscribe to model_rid and listen until <button id="unsub6">stopped</button>. Also <button id="get6">get</button> it one time
    </p>
    <p class="read-the-docs">
      <button id="btn7">7)</button> Test Indirect to Direct, <button id="unsub7">Unsub Simple</button>
    </p>
    <p class="read-the-docs">
      <button id="btn8">8)</button> Subscribe to circular_model until <button id="unsub8">stopped</button>, or <button id="get8">get</button> it once
    </p>
    <p class="read-the-docs">
      <button id="btn9">9)</button> Subscribe to unitload_collection until <button id="unsub9">stopped</button>
    </p>
    <p class="read-the-docs">
      <button id="btn10">10)</button> Subscribe to parking and cargos until <button id="unsub10">stopped</button>
    </p>
    <p class="read-the-docs">
      <button id="btn11">11)</button> Subscribe to circular_collection until <button id="unsub11">stopped</button>, or <button id="get11">get</button> it once
    </p>
    <p class="read-the-docs">
      <button id="btn12">12)</button> Subscribe to deep_collection until <button id="unsub12">stopped</button>, or <button id="get12">get</button> it once
    </p>
  </div>
`;

document.querySelector<HTMLButtonElement>("#btn1")!.onclick = () => {
  subToSimpleModel();
};

document.querySelector<HTMLButtonElement>("#btn2")!.onclick = () => {
  subToSimpleCollection();
};
document.querySelector<HTMLButtonElement>("#unsub2")!.onclick = () => {
  unsubFromSimpleCollection();
};

document.querySelector<HTMLButtonElement>("#btn3")!.onclick = () => {
  subToRandomCollection();
};
document.querySelector<HTMLButtonElement>("#unsub3")!.onclick = () => {
  unsubFromRandomCollection();
};
document.querySelector<HTMLButtonElement>("#get3")!.onclick = () => {
  getRandomCollection();
};

document.querySelector<HTMLButtonElement>("#btn4")!.onclick = () => {
  subToComplexCollection();
};
document.querySelector<HTMLButtonElement>("#unsub4")!.onclick = () => {
  unsubFromComplexCollection();
};
document.querySelector<HTMLButtonElement>("#dec4")!.onclick = () => {
  decreaseRidCollectionLength();
};
document.querySelector<HTMLButtonElement>("#inc4")!.onclick = () => {
  increaseRidCollectionLength();
};
document.querySelector<HTMLButtonElement>("#get4")!.onclick = () => {
  getRidCollection();
};

document.querySelector<HTMLButtonElement>("#btn5")!.onclick = () => {
  subToSimpleModelAndListen();
};
document.querySelector<HTMLButtonElement>("#unsub5")!.onclick = () => {
  unsubFromSimpleModel();
};

document.querySelector<HTMLButtonElement>("#btn6")!.onclick = () => {
  subToComplexModelAndListen();
};
document.querySelector<HTMLButtonElement>("#unsub6")!.onclick = () => {
  unsubFromComplexModel();
};
document.querySelector<HTMLButtonElement>("#get6")!.onclick = () => {
  getComplexModel();
};

document.querySelector<HTMLButtonElement>("#btn7")!.onclick = () => {
  testIndirectToDirect();
};
document.querySelector<HTMLButtonElement>("#unsub7")!.onclick = () => {
  unsubFromSimpleIndirect();
};

document.querySelector<HTMLButtonElement>("#btn8")!.onclick = () => {
  subToCircularModel();
};
document.querySelector<HTMLButtonElement>("#unsub8")!.onclick = () => {
  unsubFromCircularModel();
};
document.querySelector<HTMLButtonElement>("#get8")!.onclick = () => {
  getCircularModel();
};

document.querySelector<HTMLButtonElement>("#btn9")!.onclick = () => {
  subToUnitloads();
};
document.querySelector<HTMLButtonElement>("#unsub9")!.onclick = () => {
  unsubFromUnitloads();
};

document.querySelector<HTMLButtonElement>("#btn10")!.onclick = () => {
  subToLPs();
};
document.querySelector<HTMLButtonElement>("#unsub10")!.onclick = () => {
  unsubFromLPs();
};

document.querySelector<HTMLButtonElement>("#btn11")!.onclick = () => {
  subToCircularCollection();
};
document.querySelector<HTMLButtonElement>("#unsub11")!.onclick = () => {
  unsubFromCircularCollection();
};
document.querySelector<HTMLButtonElement>("#get11")!.onclick = () => {
  getCircularCollection();
};

document.querySelector<HTMLButtonElement>("#btn12")!.onclick = () => {
  subToDeepCollection();
};
document.querySelector<HTMLButtonElement>("#unsub12")!.onclick = () => {
  unsubFromDeepCollection();
};
document.querySelector<HTMLButtonElement>("#get12")!.onclick = () => {
  getDeepCollection();
};

const client = new ResClient("ws://localhost:8080", true);

// console.log("authenticating");
// console.trace(client);
(window as any).rcClient = client;
client
  .auth("ms_auth.authentication.authn", {
    "jwt-token":
      "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2OTE1ODg0MjgsImlzcyI6ImVtbWVfc3lzdGVtcyIsInN1YiI6IjAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMCJ9.llScEGJHMDZBhSy_Zo_Zx7n7r4TvqYoE45zjAWsecDv7XTZcyQyTFhTgOYMnfQpadJ7On6vvyup_f3D0Bk-lVw",
  })
  .subscribe((resp) => {
    console.log("authenticated", resp);
  });

const testObserver = (name: string) => {
  return {
    next: (v: any) =>
      console.log(`%c${name} nexted`, "color: black; font-weight: bold", v),
    error: (err: any) => console.error(`${name} errored`, err),
    complete: () =>
      console.log(`%c${name} completed`, "color: purple; font-weight:bold"),
  };
};

const tapObserver = (name: string) => {
  return tap({
    subscribe: () =>
      console.log(`%c${name} subscribed`, "color: blue; font-weight: bold"),
    unsubscribe: () =>
      console.log(`%c${name} unsubscribed`, "color: green; font-weight: bold"),
  });
};

let smID: number = 0;
let scID: number = 0;

function subToSimpleModel() {
  const name = "SimpleModel [" + ++smID + "]";

  const s1 = client
    .observe("test_resclient.model_simple.42")
    .pipe(tapObserver(name))
    .subscribe(testObserver(name));

  setTimeout(() => {
    s1.unsubscribe();
  }, 1000);
}

let s2: Subscription = new Subscription();

function subToSimpleCollection() {
  const name = "SimpleCollection [" + ++scID + "]";

  s2.add(
    client
      .observe("test_resclient.collection_int")
      .pipe(tapObserver(name))
      .subscribe(testObserver(name))
  );
}

function unsubFromSimpleCollection() {
  s2.unsubscribe();
  s2 = new Subscription();
  scID = 0;
}

let s3: Subscription = new Subscription();
let s3Counter: number = 1;

function subToRandomCollection() {
  const name = "RandomCollection [" + s3Counter++ + "]";

  s3.add(
    client
      .observe("test_resclient.collection_random")
      .pipe(debounceTime(50), tapObserver(name))
      .subscribe(testObserver(name))
  );
}

function unsubFromRandomCollection() {
  s3.unsubscribe();
  s3 = new Subscription();
  s3Counter = 1;
}

function getRandomCollection() {
  client
    .get("test_resclient.collection_random")
    .pipe(tapObserver("RandomCollection [GET]"))
    .subscribe(testObserver("RandomCollection[GET]"));
}

let s4: Subscription = new Subscription();
let s4Counter: number = 1;

function subToComplexCollection() {
  const name = "ComplexCollection [" + s4Counter++ + "]";

  s4.add(
    client
      .observe("test_resclient.collection_rid")
      .pipe(tapObserver(name))
      .subscribe(testObserver(name))
  );
}

function unsubFromComplexCollection() {
  s4.unsubscribe();
  s4 = new Subscription();
  s4Counter = 1;
}

function decreaseRidCollectionLength() {
  client.call("test_resclient.collection_rid.decrease").subscribe();
}
function increaseRidCollectionLength() {
  client.call("test_resclient.collection_rid.increase").subscribe();
}
function getRidCollection() {
  client
    .get("test_resclient.collection_rid")
    .pipe(tapObserver("ComplexCollection [GET]"))
    .subscribe(testObserver("ComplexCollection[GET]"));
}

let s5: Subscription | undefined;

function subToSimpleModelAndListen() {
  if (s5) return;

  const name = "Listen [S5]";

  s5 = client
    .observe("test_resclient.model_simple.42")
    .pipe(tapObserver(name))
    .subscribe(testObserver(name));
}
function unsubFromSimpleModel() {
  if (!s5) return;
  s5.unsubscribe();
  s5 = undefined;
}

let s6: Subscription | undefined;
function subToComplexModelAndListen() {
  if (s6) return;

  const name = "Listen [S6]";
  s6 = client
    .observe("test_resclient.model_rid.42")
    .pipe(tapObserver(name))
    .subscribe(testObserver(name));
}

function unsubFromComplexModel() {
  if (!s6) return;
  s6.unsubscribe();
  s6 = undefined;
}

function getComplexModel() {
  const name = "Get [S6]";
  client
    .get("test_resclient.model_rid.42")
    .pipe(tapObserver(name))
    .subscribe(testObserver(name));
}

let s7: Subscription;

function testIndirectToDirect() {
  const s1 = client
    .observe("test_resclient.model_rid.42")
    .pipe(tapObserver("IndirectToDirect"))
    .subscribe(testObserver("IndirectToDirect"));

  setTimeout(() => {
    s7 = client
      .observe("test_resclient.model_simple.42")
      .pipe(tapObserver("Simple"))
      .subscribe(testObserver("Simple"));

    setTimeout(() => {
      s1.unsubscribe();
    }, 3000);
  }, 1000);
}

function unsubFromSimpleIndirect() {
  if (s7) s7.unsubscribe();
}

let s8: Subscription | null = null;

function subToCircularModel() {
  if (s8 === null)
    s8 = client
      .observe("test_resclient.circular_model")
      .pipe(tapObserver("CircularSub"))
      .subscribe((v: any) => {
        console.log("CircualSub nexted", v);
        console.log("RandomProp liv. 1", v.external.updating.random_prop);
        console.log(
          "RandomProp liv. 2",
          v.external.external.external.updating.random_prop
        );
        console.log(
          "RandomProp liv. 3",
          v.external.external.external.external.external.updating.random_prop
        );
      });
  /* testObserver("CircularSub") */
}

function unsubFromCircularModel() {
  if (s8 !== null) s8.unsubscribe();
  s8 = null;
}

function getCircularModel() {
  client
    .get("test_resclient.circular_model")
    .pipe(tapObserver("CircularGet"))
    .subscribe(testObserver("CircularGet"));
}

let s9: Subscription = new Subscription();
let s9Counter: number = 1;
function subToUnitloads() {
  const num = `[${s9Counter++}]`;

  s9.add(
    client
      .observe("ms_italc.unitload_collection?sort=id")
      .pipe(tapObserver(`Unitload ${num}`))
      .subscribe(testObserver(`Unitload ${num}`))
  );

  s9.add(
    client
      .observe("ms_emme2.parking_collection")
      .pipe(tapObserver(`Parkings ${num}`))
      .subscribe(testObserver(`Parkings ${num}`))
  );
}

function unsubFromUnitloads() {
  s9.unsubscribe();
  s9 = new Subscription();
  s9Counter = 1;
}

let s10: Subscription = new Subscription();
let s10Counter: number = 1;
function subToLPs() {
  const num = `[${s10Counter++}]`;

  // s10.add(
  //   client
  //     .observe("ms_italc.lp_collection")
  //     .pipe(tapObserver(`LPs ${num}`))
  //     .subscribe(testObserver(`LPs ${num}`))
  // );

  s10.add(
    client
      .observe("ms_emme2.ulcargo_collection")
      .pipe(tapObserver(`Cargos ${num}`))
      .subscribe(testObserver(`Cargos ${num}`))
  );
}

function unsubFromLPs() {
  s10.unsubscribe();
  s10 = new Subscription();
  s10Counter = 1;
}

let s11: Subscription | undefined;

function subToCircularCollection() {
  s11 = client
    .observe("test_resclient.circular_collection")
    .pipe(tapObserver(`CircularColl`))
    .subscribe(testObserver(`CircularColl`));
}

function unsubFromCircularCollection() {
  s11?.unsubscribe();
}

function getCircularCollection() {
  client
    .get("test_resclient.circular_collection")
    .pipe(tapObserver(`CircularCollGet`))
    .subscribe(testObserver(`CircularCollGet`));
}

let s12: Subscription = new Subscription();

function subToDeepCollection() {
  s12.add(
    client
      .observe("test_resclient.deep_collection")
      .pipe(tapObserver(`DeepColl`))
      .subscribe(testObserver(`DeepColl`))
  );
}

function unsubFromDeepCollection() {
  s12.unsubscribe();
  s12 = new Subscription();
}

function getDeepCollection() {
  client
    .get("test_resclient.deep_collection")
    .pipe(tapObserver(`DeepColl`))
    .subscribe(testObserver(`DeepColl`));
}

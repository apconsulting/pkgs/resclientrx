import "solid-devtools";
import { render } from "solid-js/web";

const root = document.getElementById("solid-root");
if (root === null) throw new Error("Root element not found");

function SolidRoot() {
  // const [cache, setCache] = createStore();
  // const client = new SolidResClient("ws://localhost:8080", cache, setCache);

  // function Subscribe() {
  //   client.observe("test_resclient.collection_int").then((result) => {
  //     createEffect(() => {
  //       console.log("cache cambiata", result);
  //     });
  //   });
  // }

  return (
    <>
      <h1>IT WORKS</h1>
      {/* <button onClick={Subscribe}>Start</button> */}
      {/* <div>{cache}</div> */}
    </>
  );
}

render(() => <SolidRoot />, root);

import {
  ProperResponse,
  ResCollection,
  ResEvent,
  ResModel,
  ResRequest,
  ResResponse,
  ResResponseOrEvent,
  ResourceSet,
  WebSocketChannel,
  isDataValue,
  isProperResponse,
  isResCollectionAddEvent,
  isResCollectionRemoveEvent,
  isResResponse,
  isResponseError,
  isRid,
} from "resclient-rx";
import {
  Observable,
  first,
  lastValueFrom,
  map,
  partition,
  tap,
  timeout,
} from "rxjs";
import { Accessor, Signal } from "solid-js";

interface CachedItem<T = any> {
  directlySubscribed?: boolean;
  signal: Signal<ResModel<T>> | Signal<ResCollection>;
  memo: Accessor<T>;
}

export class SolidResClient {
  private ws: WebSocketChannel<ResResponseOrEvent, ResRequest>;

  private responseStream$: Observable<ResResponse>;
  private eventStream$: Observable<ResEvent>;

  private _msgIndex: number = 0;

  // private cache: Map<string, CachedItem> = new Map();

  cache;

  private nextID(): number {
    return ++this._msgIndex;
  }

  constructor(url: string, cache: any, setCache: any) {
    this.ws = new WebSocketChannel<ResResponseOrEvent, ResRequest>([url]);

    [this.responseStream$, this.eventStream$] = partition<
      ResResponseOrEvent,
      ResResponse
    >(this.ws, isResResponse);

    this.ws.send({
      id: this.nextID(),
      method: "version",
      params: {
        protocol: "1.2.1",
      },
    });

    this.eventStream$.subscribe((ev) => {
      const rid = ev.event.substring(0, ev.event.lastIndexOf("."));
      console.log("EVENT RID", rid);

      if (isResCollectionAddEvent(ev)) {
        console.log("ADD", ev);
        this.parseResourceSet(ev.data);
        console.log("CACHE", { ...this.cache[0] });

        const idx = ev.data.idx;
        const value = ev.data.value;

        const oldVal = [...this.cache[0][rid]];
        oldVal.splice(idx, 0, value);

        const x: any = {};
        x[rid] = oldVal;

        this.cache[1](`${rid}`, oldVal);
      } else if (isResCollectionRemoveEvent(ev)) {
        console.log("REMOVE", ev);
        const oldVal = [...this.cache[0][rid]];

        oldVal.splice(ev.data.idx, 1);

        const x: any = {};
        x[rid] = oldVal;

        this.cache[1](`${rid}`, oldVal);
      }
    });

    this.cache = [cache, setCache];
  }

  private sendRequest<Q extends ResRequest>(
    req: Q
  ): Observable<ProperResponse<Q>> {
    if (req.id === undefined) req.id = this.nextID();

    return this.responseStream$.pipe(
      tap({
        subscribe: () => {
          this.ws.send(req);
        },
      }),
      first((resp) => resp.id == req.id),
      timeout(5000),
      map((resp) => {
        if (isResponseError(resp)) throw resp.error;

        // Verifica che request e response corrispondano.
        if (!isProperResponse(req, resp)) {
          console.error("Wrong response", req, resp);
          throw new Error("Wrong response");
        } else return resp;
      })
    );
  }

  // private addToCache(
  //   rid: string,
  //   val: ResModel | ResCollection,
  //   isDirect: boolean
  // ) {
  //   if (this.cache.has(rid)) throw "Resource already in cache";

  //   const [cachedValue, setCachedValue] = createSignal(val);

  //   this.cache.set(rid, {
  //     directlySubscribed: isDirect,
  //     signal: [cachedValue, setCachedValue],
  //   });
  // }

  private parseResourceSet(rs: ResourceSet) {
    for (const [rid, model] of Object.entries({
      ...rs.models,
    })) {
      // const newItem: CachedItem = {
      //   memo: createMemo<any>(() => {
      //     const newValue: Record<string, any> = {};

      //     for (const [key, val] of Object.entries(newItem.signal[0]())) {
      //       if (isRid(val)) {
      //         const item = this.cache[0].key;

      //         if (item !== undefined) newValue[key] = item.memo();
      //         else throw `Undefined for ${key} while memoizing ${rid}`;
      //       } else if (isDataValue(val)) {
      //         newValue[key] = val.data;
      //       }
      //     }

      //     return newValue;
      //   }),

      //   signal: createSignal(resource),
      // };

      const newItem: any = {};

      for (const [key, val] of Object.entries(model)) {
        if (isRid(val)) {
          newItem[key] = this.cache[0][val.rid];
          // const item = this.cache[0].key;

          // if (item !== undefined) newValue[key] = item.memo();
          // else throw `Undefined for ${key} while memoizing ${rid}`;
        } else if (isDataValue(val)) {
          newItem[key] = val.data;
        } else newItem[key] = val;
      }

      const x: any = {};
      x[rid] = newItem;

      this.cache[1](x);
    }

    for (const [rid, collection] of Object.entries({
      ...rs.collections,
    })) {
      const newArr: any[] = [];

      for (const val of collection) {
        if (isRid(val)) {
          newArr.push(this.cache[0][val.rid]);
        } else if (isDataValue(val)) {
          newArr.push(val.data);
        } else {
          newArr.push(val);
        }
      }

      const x: any = {};
      x[rid] = newArr;

      this.cache[1](x);
    }
  }

  auth<T>(method: string, params: any): Accessor<T> {
    throw "Not implemented";
  }

  call<T>(rid: string): Accessor<T> {
    throw "Not implemented";
  }

  get<T>(rid: string): Accessor<T> {
    throw "Not implemented";
  }

  async observe<T>(rid: string): Promise<Accessor<T>> {
    if (this.cache[0][rid]) return this.cache[0][rid];

    const res = await lastValueFrom(
      this.sendRequest({
        id: this.nextID(),
        method: `subscribe.${rid}`,
      })
    );

    console.log("res.result", res.result);
    this.parseResourceSet(res.result);
    console.log("cache", { ...this.cache[0] });

    const item = this.cache[0][rid];

    console.log("item", item);
    if (item === undefined)
      throw `Requested resource ${rid} missing in response`;

    return item;
  }
}
